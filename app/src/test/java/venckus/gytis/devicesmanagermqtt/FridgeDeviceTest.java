package venckus.gytis.devicesmanagermqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.sql.Time;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import Devices.Device;
import Devices.Fridge_Device;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Fridge_Device.class})
public class FridgeDeviceTest {

    Device device = new Fridge_Device();
    Device spiedClass;
    String Id = device.getName();
    String Room = device.getRoom();
    String broker = device.getBroker();
    String UserId = "521829b4c3d58fb188f6eada1a785dd16a2102d9";
    public MqttClient mqttClient;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(Fridge_Device.class);
        spiedClass= PowerMockito.spy(new Fridge_Device());
        PowerMockito.when(spiedClass.formJSONObject()).thenCallRealMethod();
    }
    @Test
    public void connectTest() throws MqttException {
        int qos = 1;
        MqttClient mqttClient;
        MqttConnectOptions connOpts;

        mqttClient = new MqttClient(broker, Id, new MemoryPersistence());
        connOpts = new MqttConnectOptions();
        connOpts.setUserName("bpnegedw");
        connOpts.setPassword("4t-MJwH7Dc27".toCharArray());
        connOpts.setCleanSession(true);
        connOpts.setKeepAliveInterval(1000);
        connOpts.setWill(String.format(UserId + "/devices/%s/%s", Room, Id), "offline".getBytes(), 1, true);
        mqttClient.connect(connOpts);

        assertTrue(mqttClient.isConnected());
      //  assertEquals("offline".getBytes(), connOpts.getWillMessage().getPayload());
    }

    @Test
    public void publishPrototypesTest() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void publishDeviceStatusTest() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void publishDCTest() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void subscribeToCommandsTest() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void parseCommandsFromJSONTest() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void formJSONObjectTest() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void formDeviceStatusJSONObjectTest() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void disconnectTest() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void turnOnTest() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void turnOffTest() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void setUserIdTest() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void getNameTest() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void getRoomTest() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void getStateTest() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void controlFridgeTemperature() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void controlFreezerTemperature() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void controlLock() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }
}