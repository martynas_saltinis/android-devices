package venckus.gytis.devicesmanagermqtt;

import org.junit.Test;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class DeviceManagerTest {

    @Test
    public void addDevice() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void removeDevice() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void getAvailableDevices() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void removeMultipleDevices() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }

    @Test
    public void getDevicebySwitch() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
    }
}