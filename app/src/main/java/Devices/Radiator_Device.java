package Devices;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Radiator_Device extends Device{
    private int MinTemp = 20;
    private int MaxTemp = 45;
    private int increment = 1;
    private int CurrentTemp = setDefaultTemperature(MinTemp, MaxTemp);

    public Radiator_Device() {
        Name = "Radiator";
        Room = "livingroom";
        this.State = true;
    }

    @Override
    public JSONObject formJSONObject(){
        JSONObject deviceObj = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        JSONObject parametersObj = new JSONObject();
        try {
            deviceObj.put("name", Name);
            parametersObj.put("name", "ControlHeating");
            obj.put("displayName", "Radiator Temperature");
            obj.put("currentValue", CurrentTemp);
            obj.put("intervalStart", MinTemp);
            obj.put("intervalEnd", MaxTemp);
            obj.put("increment", increment);
            obj.put("type", "slider");
            obj.put("units", "°C");
            JSONArray tempArray = new JSONArray();
            tempArray.put(obj);
            parametersObj.put("parameters", tempArray);
            array.put(parametersObj);
            obj = new JSONObject();
            obj.put("name", "TurnOn");
            array.put(obj);
            obj = new JSONObject();
            obj.put("name", "TurnOff");
            array.put(obj);
            deviceObj.put("functions", array);
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return deviceObj;
    }

    @Override
    public JSONObject formDeviceStatusJSONObject(){
        JSONObject deviceObj = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        try {
            obj.put("displayName", "Radiator Temperature");
            obj.put("currentValue", CurrentTemp);
            obj.put("units", "°C");
            array.put(obj);
            deviceObj.put("deviceStatus", array);
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return deviceObj;
    }

    public void parseCommandsFromJSON(String topic, MqttMessage message) throws JSONException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
      super.parseCommandsFromJSON(topic, message);
    }

    private int setDefaultTemperature(int minTemp, int maxTemp){
        return ((minTemp + maxTemp) / 2);
    }

    public void ControlHeating(String value){
        CurrentTemp = Integer.parseInt(value);
        System.out.println("Temperature is set to " + CurrentTemp);
    }
}
