package Devices;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Lamp_Device extends Device{
    private int MinPower = 10;
    private int MaxPower = 150;
    private int increment = 1;
    private int CurrentPower = setDefault(MinPower, MaxPower);

    public Lamp_Device() {
        Name = "Lamp";
        Room = "corridor";
        this.State = true;
    }

    @Override
    public JSONObject formJSONObject(){
        JSONObject deviceObj = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        JSONObject parametersObj = new JSONObject();
        try {
            deviceObj.put("name", Name);
            parametersObj.put("name", "ControlPower");
            obj.put("displayName", "Lamp Power");
            obj.put("currentValue", CurrentPower);
            obj.put("intervalStart", MinPower);
            obj.put("intervalEnd", MaxPower);
            obj.put("increment", increment);
            obj.put("type", "slider");
            obj.put("units", "W");
            JSONArray tempArray = new JSONArray();
            tempArray.put(obj);
            parametersObj.put("parameters", tempArray);
            array.put(parametersObj);
            obj = new JSONObject();
            obj.put("name", "TurnOn");
            array.put(obj);
            obj = new JSONObject();
            obj.put("name", "TurnOff");
            array.put(obj);
            deviceObj.put("functions", array);
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return deviceObj;
    }

    @Override
    public JSONObject formDeviceStatusJSONObject(){
        JSONObject deviceObj = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        try {
            obj.put("displayName", "Lamp Power");
            obj.put("currentValue", CurrentPower);
            obj.put("units", "W");
            array.put(obj);
            deviceObj.put("deviceStatus", array);
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return deviceObj;
    }

    public void parseCommandsFromJSON(String topic, MqttMessage message) throws JSONException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
        super.parseCommandsFromJSON(topic, message);
    }

    private int setDefault(int minValue, int maxValue){
        return ((minValue + maxValue) / 2);
    }

    public void ControlPower(String value){
        CurrentPower = Integer.parseInt(value);
        System.out.println("Power is set to " + CurrentPower);
    }
}
