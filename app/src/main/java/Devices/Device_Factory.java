package Devices;

public class Device_Factory {
    public Device getDevice(String deviceType){
        if (deviceType == null)
            return null;
        switch (deviceType){
            case "Fridge":
                return new Fridge_Device();
            case "TV":
                return new TV_Device();
            case "Radiator":
                return new Radiator_Device();
            case "AC":
                return new AirConditioner_Device();
            case "RVC":
                return new RobotVacuumCleaner_Device();
            case "Lamp":
                return new Lamp_Device();
        }
        return null;
    }
}
