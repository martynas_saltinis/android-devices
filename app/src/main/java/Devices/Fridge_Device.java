package Devices;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class Fridge_Device extends Device {
    private int FridgeMinTemp = 1;
    private int FridgeMaxTemp = 5;
    private int FridgeCurrentTemp = 2;

    private int increment = 1;
    private int FreezerMinTemp = -30;
    private int FreezerMaxTemp = -12;
    private int FreezerCurrentTemp = -18;

    private String FridgeLock = "unlocked";

    public Fridge_Device() {
        this.Name = "Fridge";
        this.Room = "Kitchen";
        this.State = true;
    }

    @Override
    public JSONObject formJSONObject(){
        JSONObject deviceObj = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        JSONObject parametersObj = new JSONObject();
        try {
            deviceObj.put("name", Name);
            parametersObj.put("name", "ControlLock");
            obj.put("displayName", "Fridge lock state");
            obj.put("currentValue", FridgeLock);
            JSONArray statesArray = new JSONArray();
            statesArray.put("locked");
            statesArray.put("unlocked");
            obj.put("states", statesArray);
            obj.put("type", "switch");
            JSONArray tempArray = new JSONArray();
            tempArray.put(obj);
            parametersObj.put("parameters", tempArray);
            array.put(parametersObj);
            obj = new JSONObject();
            parametersObj = new JSONObject();
            parametersObj.put("name", "ControlFridgeTemperature");
            obj.put("displayName", "Fridge temperature");
            obj.put("currentValue", FridgeCurrentTemp);
            obj.put("intervalStart", FridgeMinTemp);
            obj.put("intervalEnd", FridgeMaxTemp);
            obj.put("increment", increment);
            obj.put("type", "slider");
            obj.put("units", "°C");
            tempArray = new JSONArray();
            tempArray.put(obj);
            parametersObj.put("parameters", tempArray);
            array.put(parametersObj);
            obj = new JSONObject();
            parametersObj = new JSONObject();
            parametersObj.put("name", "ControlFreezerTemperature");
            obj.put("displayName", "Freezer temperature");
            obj.put("currentValue", FreezerCurrentTemp);
            obj.put("intervalStart", FreezerMinTemp);
            obj.put("intervalEnd", FreezerMaxTemp);
            obj.put("increment", increment);
            obj.put("type", "slider");
            obj.put("units", "°C");
            tempArray = new JSONArray();
            tempArray.put(obj);
            parametersObj.put("parameters", tempArray);
            array.put(parametersObj);
            obj = new JSONObject();
            obj.put("name", "TurnOn");
            array.put(obj);
            obj = new JSONObject();
            obj.put("name", "TurnOff");
            array.put(obj);
            deviceObj.put("functions", array);
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return deviceObj;
    }

    public void parseCommandsFromJSON(String topic, MqttMessage message) throws JSONException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
        super.parseCommandsFromJSON(topic, message);
    }

    @Override
    public JSONObject formDeviceStatusJSONObject(){
        JSONObject deviceObj = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        try {
            obj.put("displayName", "Fridge Temperature");
            obj.put("currentValue", FridgeCurrentTemp);
            obj.put("units", "°C");
            array.put(obj);
            obj = new JSONObject();
            obj.put("displayName","Freezer Temperature");
            obj.put("currentValue", FreezerCurrentTemp);
            obj.put("units", "°C");
            array.put(obj);
            obj = new JSONObject();
            obj.put("displayName","Fridge Lock");
            obj.put("currentValue", FridgeLock);
            array.put(obj);
            deviceObj.put("deviceStatus", array);
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return deviceObj;
    }

    private int setDefaultTemperature(int minTemp, int maxTemp){
        return ((minTemp + maxTemp) / 2);
    }

    public void ControlFridgeTemperature(String temperature) {
        FridgeCurrentTemp = Integer.parseInt(temperature);
        System.out.println("Temperature is set to " + FridgeCurrentTemp);
    }


    public void ControlFreezerTemperature(String temperature) {
        FreezerCurrentTemp = Integer.parseInt(temperature);
        System.out.println("Temperature is set to " + FreezerCurrentTemp);
    }

    public void ControlLock(String value){
        FridgeLock = value;
    }
}
