package Devices;


import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;

public class TV_Device extends Device {
    private int minVolume = 0;
    private int maxVolume = 100;
    private int minChannel = 0;
    private int maxChannel = 100;
    private int increment = 1;

    private int CurrentChannel = 73;
    private int CurrentVolume = 20;

    public TV_Device(){
        Name = "TV";
        Room = "livingroom";
        this.State = true;
    }

    @Override
    public JSONObject formJSONObject(){
        JSONObject deviceObj = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        JSONObject parametersObj = new JSONObject();
        try {
            deviceObj.put("name", Name);
            parametersObj.put("name", "ControlVolume");
            obj.put("displayName", "TV Volume");
            obj.put("currentValue", CurrentVolume);
            obj.put("intervalStart", minVolume);
            obj.put("intervalEnd", maxVolume);
            obj.put("increment", increment);
            obj.put("type", "slider");
            JSONArray tempArray = new JSONArray();
            tempArray.put(obj);
            parametersObj.put("parameters", tempArray);
            array.put(parametersObj);
            obj = new JSONObject();
            parametersObj = new JSONObject();
            parametersObj.put("name", "ControlChannel");
            obj.put("displayName", "TV Channel");
            obj.put("currentValue", CurrentChannel);
            obj.put("intervalStart", minChannel);
            obj.put("intervalEnd", maxChannel);
            obj.put("increment", increment);
            obj.put("type", "integer");
            tempArray = new JSONArray();
            tempArray.put(obj);
            parametersObj.put("parameters", tempArray);
            array.put(parametersObj);
            obj = new JSONObject();
            obj.put("name", "TurnOn");
            array.put(obj);
            obj = new JSONObject();
            obj.put("name", "TurnOff");
            array.put(obj);
            deviceObj.put("functions", array);
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return deviceObj;
    }

    @Override
    public JSONObject formDeviceStatusJSONObject(){
        JSONObject deviceObj = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        try {
            obj.put("displayName", "TV Volume");
            obj.put("currentValue", CurrentVolume);
            array.put(obj);
            obj = new JSONObject();
            obj.put("displayName","TV Channel");
            obj.put("currentValue", CurrentChannel);
            array.put(obj);
            deviceObj.put("deviceStatus", array);
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return deviceObj;
    }

    public void parseCommandsFromJSON(String topic, MqttMessage message) throws JSONException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
        super.parseCommandsFromJSON(topic, message);
    }

    public void ControlChannel(String value) {
        CurrentChannel = Integer.parseInt(value);
        System.out.println("Channel is set to " + CurrentChannel);
    }


    public void ControlVolume(String value) {
        CurrentVolume = Integer.parseInt(value);
        System.out.println("Volume is set to " + CurrentVolume);
    }
}
