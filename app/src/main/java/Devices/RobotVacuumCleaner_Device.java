package Devices;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;

public class RobotVacuumCleaner_Device extends Device{

    private boolean wetMopping;
    private boolean active;
    private enum RVC_States {
        GENERAL,
        SCHEDULED,
        TURBO,
        REMOTE
    }
    private String currentMoppingMode;

    public RobotVacuumCleaner_Device() {
        wetMopping = true;
        active = false;
        currentMoppingMode = RVC_States.GENERAL.toString();
        Name = "Robot Vacuum Cleaner";
        Room = "corridor";
        State = true;
    }

    @Override
    public JSONObject formJSONObject(){
        JSONObject deviceObj = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        JSONObject parametersObj = new JSONObject();
        try {
            deviceObj.put("name", Name);
            parametersObj.put("name", "ControlWetMopping");
            obj.put("displayName", "Wet Mopping");
            obj.put("currentValue", wetMopping);
            obj.put("type", "checkbox");
            JSONArray tempArray = new JSONArray();
            tempArray.put(obj);
            parametersObj.put("parameters", tempArray);
            array.put(parametersObj);
            obj = new JSONObject();
            parametersObj = new JSONObject();
            parametersObj.put("name", "ControlMode");
            obj.put("displayName", "Mopping mode");
            obj.put("currentValue", currentMoppingMode);
            JSONArray modeStatesArray = new JSONArray();
            for (RVC_States state : RVC_States.values())
            modeStatesArray.put(state.toString());
            obj.put("options", modeStatesArray);
            obj.put("type", "list");
            tempArray = new JSONArray();
            tempArray.put(obj);
            parametersObj.put("parameters", tempArray);
            array.put(parametersObj);
            obj = new JSONObject();
            parametersObj = new JSONObject();
            parametersObj.put("name", "ControlPower");
            obj.put("displayName", "Power enabled");
            obj.put("currentValue", active);
            JSONArray powerStatesArray = new JSONArray();
            powerStatesArray.put("on");
            powerStatesArray.put("off");
            obj.put("states", powerStatesArray);
            obj.put("type", "switch");
            tempArray = new JSONArray();
            tempArray.put(obj);
            parametersObj.put("parameters", tempArray);
            array.put(parametersObj);
            obj = new JSONObject();
            obj.put("name", "TurnOn");
            array.put(obj);
            obj = new JSONObject();
            obj.put("name", "TurnOff");
            array.put(obj);
            deviceObj.put("functions", array);
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return deviceObj;
    }

    @Override
    public JSONObject formDeviceStatusJSONObject(){
        JSONObject deviceObj = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        try {
            obj.put("displayName", "Power enabled");
            obj.put("currentValue", active);
            array.put(obj);
            obj = new JSONObject();
            obj.put("displayName", "Wet Mopping");
            obj.put("currentValue", wetMopping);
            array.put(obj);
            obj = new JSONObject();
            obj.put("displayName", "Mopping mode");
            obj.put("currentValue", currentMoppingMode);
            array.put(obj);
            deviceObj.put("deviceStatus", array);
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return deviceObj;
    }

    public void parseCommandsFromJSON(String topic, MqttMessage message) throws JSONException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
        super.parseCommandsFromJSON(topic, message);
    }

    public void UseWetMopping(String value) {
        wetMopping = value.equals("on");
    }

    public void ControlPower(String value) {
        if (value.equals("on")){
            active = true;
        }
        else
            active = false;
    }

    public void ControlMode(String value){
        currentMoppingMode = value;
    }

    public void DownloadSoftwareUpdates(){

    }
}
