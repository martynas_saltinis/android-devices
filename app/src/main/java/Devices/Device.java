package Devices;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Device implements MqttCallback, Serializable {
    public String Id;
    public String Room;
    public String Name;
    public String UserId;
    public boolean State = false;
    //Broker connection
    public String broker = "tcp://m24.cloudmqtt.com:15033";
  //  public String localBroker = "tcp://192.168.42.250:1883";
    public byte [] LWT;
    public int qos = 1;
    public transient MqttClient mqttClient;
    public transient MqttConnectOptions connOpts;
    public String functionsTopic;
    public String commandsTopic;

    public void Connect() throws MqttException {
        Id =  getName();//mqttClient.generateClientId().substring(4);
        Room = getRoom();
        LWT = ("LWT: Device " + this.getName() + " disconnected unexpectedly").getBytes();
        mqttClient = new MqttClient(broker, Id, new MemoryPersistence());
        connOpts = new MqttConnectOptions();
        connOpts.setUserName("bpnegedw");
        connOpts.setPassword("4t-MJwH7Dc27".toCharArray());
        connOpts.setCleanSession(true);
        connOpts.setKeepAliveInterval(1000);
        connOpts.setWill(String.format(UserId + "/devices/%s/%s", Room, Id), "offline".getBytes(), 1, true);
        mqttClient.setCallback(this);
        mqttClient.connect(connOpts);
    }

    public void PublishPrototypes() throws MqttException {
        JSONObject obj = formJSONObject();
        MqttMessage message = new MqttMessage();
        message.setPayload(obj.toString().getBytes());
        message.setQos(qos);
        message.setRetained(false);
        functionsTopic = String.format(UserId + "/devices/%s/%s/functions", Room, Id);
        MqttTopic topic = mqttClient.getTopic(functionsTopic);
        topic.publish(message);
    }

    public void PublishDeviceStatus() throws MqttException {
        JSONObject obj = formDeviceStatusJSONObject();
        MqttMessage message = new MqttMessage();
        message.setPayload(obj.toString().getBytes());
        message.setQos(qos);
        message.setRetained(true);
        String deviceInfoTopic = String.format(UserId + "/devices/%s/%s", Room, Id);
        MqttTopic topic = mqttClient.getTopic(deviceInfoTopic);
        topic.publish(message);
    }

    public void PublishDC() throws MqttException {
        MqttMessage message = new MqttMessage();
        message.setPayload("offline".getBytes());
        message.setQos(qos);
        message.setRetained(true);
        String deviceInfoTopic = String.format(UserId+"/devices/%s/%s", Room, Id);
        MqttTopic topic = mqttClient.getTopic(deviceInfoTopic);
        topic.publish(message);
    }

    public void SubscribeToCommands() throws MqttException {
        commandsTopic = String.format(UserId + "/devices/%s/%s/commands", Room, Id);
        mqttClient.subscribe(commandsTopic, 1);
    }

    @Override
    public void connectionLost(Throwable cause) {
        System.out.println("Connection lost");
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        parseCommandsFromJSON(topic, message);
        PublishDeviceStatus();
    }

    public void parseCommandsFromJSON(String topic, MqttMessage message) throws JSONException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
        Class<?> c = Class.forName("Devices."+this.getClass().getSimpleName());
        Class[] argTypes = new Class[] { String[].class };
        JSONObject obj = new JSONObject(message.toString());
        String command = obj.getString("name");
        System.out.println("Topic: " + topic  + ", command: " + command);
        if (command.equals("TurnOff")) {
            this.State = false;
        }
        else if (command.equals("TurnOn")){
                //TODO do something
        } else {
            JSONArray parameters = obj.getJSONArray("parameters");
            String [] args = new String[parameters.length()];
            for (int i = 0; i < parameters.length(); i++) {
                String value = parameters.getJSONObject(i).getString("value");
                args[i] = value;
            }
            if (args.length == 1){
                Method executableMethod = c.getMethod(command, String.class);
                executableMethod.invoke(this, args[0]);
            } else {
                Method executableMethod = c.getMethod(command, argTypes);
                executableMethod.invoke(this, (Object) args);
            }
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }

    public JSONObject formJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("Empty json", -1);
        } catch (JSONException e){
            e.printStackTrace();
        }
        return obj;
    }

    public JSONObject formDeviceStatusJSONObject(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("Empty json", -1);
        } catch (JSONException e){
            e.printStackTrace();
        }
        return obj;
    }

    public void Disconnect(){
        try {
            PublishDC();
            mqttClient.disconnect();
        } catch (MqttException e){
            e.printStackTrace();
        }
    }

    public void TurnOn() {
        try {
            Connect();
            PublishPrototypes();
            PublishDeviceStatus();
            SubscribeToCommands();
        } catch (MqttException e){
            e.printStackTrace();
        }
    }

    public void TurnOff() {
        System.out.println(this.getName() + " disconnected");
        Disconnect();
    }

    public void setUserId(String userId){
        this.UserId = userId;
    }

    public String getName(){
        return this.Name.toLowerCase().replaceAll("\\s+","");
    }

    public String getRoom(){
        return this.Room.toLowerCase().replaceAll("\\s+","");
    }

    public boolean getState(){
        return this.State;
    }

    public String getBroker(){
        return this.broker;
    }

    public String getId(){
        return this.Id;
    }
}
