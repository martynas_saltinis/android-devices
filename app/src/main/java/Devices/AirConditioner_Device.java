package Devices;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;

public class AirConditioner_Device extends Device{
    private int MinTemp = 10;
    private int MaxTemp = 30;
    private int CurrentTemp = setDefaultTemperature(MinTemp, MaxTemp);

    public AirConditioner_Device() {
        Name = "Air Conditioner";
        Room = "livingroom";
        this.State = true;
    }

    @Override
    public JSONObject formJSONObject(){
        JSONObject deviceObj = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        JSONObject parametersObj = new JSONObject();
        try {
            deviceObj.put("name", Name);
            parametersObj.put("name", "SetTemperature");
            obj.put("displayName", "AC Temperature");
            obj.put("currentValue", CurrentTemp);
            obj.put("intervalStart", MinTemp);
            obj.put("intervalEnd", MaxTemp);
            obj.put("type", "integer");
            obj.put("units", "°C");
            JSONArray tempArray = new JSONArray();
            tempArray.put(obj);
            parametersObj.put("parameters", tempArray);
            array.put(parametersObj);
            obj = new JSONObject();
            obj.put("name", "TurnOn");
            array.put(obj);
            obj = new JSONObject();
            obj.put("name", "TurnOff");
            array.put(obj);
            deviceObj.put("functions", array);
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return deviceObj;
    }

    @Override
    public JSONObject formDeviceStatusJSONObject(){
        JSONObject deviceObj = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        try {
            obj.put("displayName", "AC Temperature");
            obj.put("currentValue", CurrentTemp);
            obj.put("units", "°C");
            array.put(obj);
            deviceObj.put("deviceStatus", array);
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return deviceObj;
    }

    public void parseCommandsFromJSON(String topic, MqttMessage message) throws JSONException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
        super.parseCommandsFromJSON(topic, message);
    }

    public void SetTemperature(String value) {
        CurrentTemp = Integer.parseInt(value);
        System.out.println("Temperature is set to " + CurrentTemp);
    }

    private int setDefaultTemperature(int minTemp, int maxTemp){
        return ((minTemp + maxTemp) / 2);
    }
}
