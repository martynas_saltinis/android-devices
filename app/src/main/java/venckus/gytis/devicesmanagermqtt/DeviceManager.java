package venckus.gytis.devicesmanagermqtt;

import android.widget.Switch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Devices.Device;

public class DeviceManager {
    private Map<Device, Switch> availableDevices;
    private Map<Device, Switch> removedDevices;

    public DeviceManager(){
        availableDevices = new HashMap<>();
    }

    public void addDevice(Device device, Switch switchBtn){
        availableDevices.put(device, switchBtn);
    }
    public void removeDevice(Device device){
//        removedDevices.put(device, availableDevices.get(device));
        availableDevices.remove(device);
    }

    public Map<Device, Switch> getAvailableDevices(){
        return availableDevices;
    }

    public Device getDevicebySwitch(Switch sw){
        for (Device device : availableDevices.keySet()) {
            if (availableDevices.get(device).equals(sw)){
                return device;
            }
        }
        return null;
    }
}
