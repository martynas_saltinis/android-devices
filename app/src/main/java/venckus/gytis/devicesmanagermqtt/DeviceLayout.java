package venckus.gytis.devicesmanagermqtt;

import android.content.res.Resources;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import Devices.Device;

public class DeviceLayout extends AppCompatActivity {

    Device device;
    Handler handler;
    Map<String, String> addedParameters;
    Map<String, String> addedViews;
    List<JSONObject> parameters;
    public JSONObject deviceInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        parameters = new ArrayList<>();

        addedParameters = new HashMap<>();
        addedViews = new HashMap<>();

        setContentView(R.layout.activity_device_layout);
        device = MainActivity.focusedDevice;
        deviceInfo = device.formJSONObject();
        DisplayViews(getParameters(deviceInfo));
        Update();
        System.out.print("test");
    }

    private void Update() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run () {
                deviceInfo = device.formJSONObject();
                System.out.println(deviceInfo);
                DisplayViews(getParameters(deviceInfo));
                Update();
            }
        },1000);
    }

    private List<JSONObject> getParameters(JSONObject deviceInfo){
        List <JSONObject> params = new ArrayList<>();
        try {
            JSONArray functions = deviceInfo.getJSONArray("functions");
            for (int i = 0; i < functions.length(); i++) {
                JSONObject function = functions.getJSONObject(i);
                    JSONArray parameters = function.getJSONArray("parameters");
                    for (int j = 0; j < parameters.length(); j++) {
                        JSONObject parameter = parameters.getJSONObject(j);
                        params.add(parameter);
                    }
            }
        } catch (JSONException e){
         //   e.printStackTrace();
        }
        return params;
    }

    private void DisplayViews(List<JSONObject> parameters){
        LinearLayout linearLayout = findViewById(R.id.linearLayout);
        if (linearLayout.getChildCount() != 0)
            linearLayout.removeAllViews();

        try {
            for (JSONObject parameter : parameters) {
                switch (parameter.get("type").toString()) {
                    case ("slider"):
                        generateSliderView(linearLayout, parameter);
                        addedParameters.put(parameter.get("displayName").toString(), String.valueOf(parameter.getInt("currentValue")));
                        break;

                    case ("integer"):
                        generateIntegerView(linearLayout, parameter);
                        addedParameters.put(parameter.get("displayName").toString(), String.valueOf(parameter.getInt("currentValue")));
                        break;
                    case ("switch"):
                        generateSwitchView(linearLayout, parameter);
                        break;
                    case ("list"):
                        generateSwitchView(linearLayout, parameter);
                        break;
                    case ("checkbox"):
                        generateCheckboxView(linearLayout, parameter);
                        break;
                }
            }
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void generateSliderView(LinearLayout linearLayout, JSONObject parameter) throws JSONException{
        TextView name = new TextView(this);
        TextView value = new TextView(this);
        ProgressBar pb = new ProgressBar(this, null, android.R.attr.progressBarStyleHorizontal);
        pb.setId(View.generateViewId());
        pb.setScaleY(3f);
        pb.setMax(Math.abs(parameter.getInt("intervalStart") + parameter.getInt("intervalEnd")));
        pb.setProgress(Math.abs(parameter.getInt("currentValue")));
        name.setId(View.generateViewId());
        name.setText(parameter.get("displayName").toString());
        name.setTextSize(20);
        value.setId(View.generateViewId());
        try {
            value.setText((parameter.getInt("currentValue") > 0) ? String.valueOf(pb.getProgress() + parameter.getString("units")) : String.valueOf(pb.getProgress() * -1 + parameter.getString("units")));
        } catch (JSONException e){
            value.setText((parameter.getInt("currentValue") > 0) ? String.valueOf(pb.getProgress()) : String.valueOf(pb.getProgress() * -1));
        }
        value.setGravity(Gravity.CENTER);
        value.setPadding(0,0,0, 100);
        linearLayout.addView(name);
        linearLayout.addView(pb);
        linearLayout.addView(value);
        addedViews.put(pb.getClass().getSimpleName(), "R.id."+String.valueOf(pb.getId()));
        addedViews.put(value.getClass().getSimpleName(), "R.id."+String.valueOf(value.getId()));
    }

    private void generateIntegerView(LinearLayout linearLayout, JSONObject parameter) throws JSONException{
        TextView name = new TextView(this);
        TextView value = new TextView(this);
        name.setId(View.generateViewId());
        name.setText(parameter.get("displayName").toString());
        name.setTextSize(20);
        value.setId(View.generateViewId());
        try {
            value.setText((String.valueOf(parameter.getInt("currentValue"))) + parameter.getString("units"));
        } catch (JSONException e){
            value.setText(String.valueOf(parameter.getInt("currentValue")));
        }
        value.setTextSize(32);
        value.setGravity(Gravity.CENTER);
        value.setPadding(0,0,0, 100);
        linearLayout.addView(name);
        linearLayout.addView(value);
        addedViews.put(value.getClass().getSimpleName(), "R.id."+String.valueOf(value.getId()));
    }

    private void generateSwitchView(LinearLayout linearLayout, JSONObject parameter) throws JSONException {
        TextView state = new TextView(this);
        state.setId(View.generateViewId());
        state.setText(parameter.get("displayName").toString() + ": " + parameter.getString("currentValue"));
        state.setTextSize(20);
        state.setPadding(0,0,0, 100);
        linearLayout.addView(state);
    }

    private void generateCheckboxView(LinearLayout linearLayout, JSONObject parameter) throws JSONException {
        TextView state = new TextView(this);
        CheckBox checkBox = new CheckBox(this);
        checkBox.setClickable(false);
        state.setId(View.generateViewId());
        checkBox.setId(View.generateViewId());
        state.setText(parameter.get("displayName").toString());
        if (parameter.getString("currentValue").equals("true"))
            checkBox.setChecked(true);
        else
            checkBox.setChecked(false);
        state.setTextSize(20);
        checkBox.setX(Resources.getSystem().getDisplayMetrics().widthPixels / 2 - checkBox.getWidth());
        checkBox.setPadding(0,0,0, 100);
        linearLayout.addView(state);
        linearLayout.addView(checkBox);
    }

    private void UpdateViews(Map<String, String> views, List<JSONObject> parameters) {
        Set<String> keySet = views.keySet();
        try {
            for (JSONObject parameter : parameters) {
                for (String viewClass : keySet) {
                    switch (viewClass) {
                        case "ProgressBar":
                            ProgressBar progressBar = findViewById(Integer.parseInt(views.get(viewClass)));
                            progressBar.setProgress(Math.abs(parameter.getInt("currentValue")));
                            break;

                        case "TextView":
                            TextView textView = findViewById(Integer.parseInt(views.get(viewClass)));
                            textView.setText((parameter.getInt("currentValue") > 0) ? String.valueOf(parameter.getInt("currentValue")) : String.valueOf(parameter.getInt("currentValue") * -1));
                            break;

                            default:
                                continue;
                    }
                }
            }
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

}
