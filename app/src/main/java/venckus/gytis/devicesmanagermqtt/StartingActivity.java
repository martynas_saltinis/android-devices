package venckus.gytis.devicesmanagermqtt;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class StartingActivity extends AppCompatActivity {

    EditText username;
    Button getUserIdButton;
    ServerRequests serverRequests;
    String UserId = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting);
        username = findViewById(R.id.username);
        getUserIdButton = findViewById(R.id.getUserIdButton);
        serverRequests = new ServerRequests();

        getUserIdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                serverRequests.SendRequest_GET(username.getText().toString());
                UserId = serverRequests.getUserId();
                if (UserId == null){
                    Toast.makeText(getApplicationContext(), "User not found!", Toast.LENGTH_SHORT).show();
                } else
                    LogIn(username.getText().toString());
            }
        });
    }

    private void LogIn(String username){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("USER INFO", UserId + "/" + username);
        startActivity(intent);
    }
}
