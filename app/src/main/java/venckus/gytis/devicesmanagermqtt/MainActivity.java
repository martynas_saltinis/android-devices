package venckus.gytis.devicesmanagermqtt;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import Devices.Device;
import Devices.Device_Factory;

public class MainActivity extends AppCompatActivity {

    DeviceManager deviceManager;
    Map<Switch, Button> viewsMap;
    Device_Factory deviceFactory;
    String UserId;
    TextView usernameHolder;
    public static Device focusedDevice;

    Device lampDevice;
    Button lampInfoButton;
    Switch lampSwitchButton;

    Device fridgeDevice;
    Button fridgeInfoButton;
    Switch fridgeSwitchButton;

    Device tvDevice;
    Button tvInfoButton;
    Switch tvSwitchButton;

    Device radiatorDevice;
    Button radiatorInfoButton;
    Switch radiatorSwitchButton;

    Device acDevice;
    Button acInfoButton;
    Switch acSwitchButton;

    Device rvcDevice;
    Button rvcInfoButton;
    Switch rvcSwitchButton;

    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String userInfoIntent = getIntent().getStringExtra("USER INFO");
        UserId = userInfoIntent.substring(0, userInfoIntent.lastIndexOf('/'));
        String username = userInfoIntent.substring(userInfoIntent.lastIndexOf('/')+1);
        deviceManager = new DeviceManager();
        handler = new Handler();
        deviceFactory = new Device_Factory();
        InitializeViews();
        usernameHolder.setText("User: " + username);
        InitializeMap();

        FridgeButtonLogic();
        TVButtonLogic();
        RadiatorButtonLogic();
        AcButtonLogic();
        rvcButtonLogic();
        lampButtonLogic();
        InfoButtonLogic(viewsMap);
        Update();
    }

    @Override
    public void onBackPressed() {
        //TODO add logic or prompt exit screen
    }

    private void Update() {
    handler.postDelayed(new Runnable() {
        @Override
        public void run () {
            Map<Device, Switch> availableDevices = deviceManager.getAvailableDevices();
            Iterator<Device> iterator = availableDevices.keySet().iterator();
            while (iterator.hasNext()){
                Device device = iterator.next();
                if (!device.getState()) {
                    device.TurnOff();
                    availableDevices.get(device).setChecked(false);
                    iterator.remove();
                }
            }
            Update();
        }
    },500);
}

    private void FridgeButtonLogic(){
        fridgeSwitchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if (fridgeSwitchButton.isChecked()) {
                    fridgeDevice = deviceFactory.getDevice("Fridge");
                    fridgeDevice.setUserId(UserId);
                    deviceManager.addDevice(fridgeDevice, fridgeSwitchButton);
                    fridgeDevice.TurnOn();
                }
                else {
                    fridgeDevice.TurnOff();
                    deviceManager.removeDevice(fridgeDevice);
                }
            }
        });
    }

    private void TVButtonLogic(){
        tvSwitchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if (tvSwitchButton.isChecked()) {
                    tvDevice = deviceFactory.getDevice("TV");
                    tvDevice.setUserId(UserId);
                    deviceManager.addDevice(tvDevice, tvSwitchButton);
                    tvDevice.TurnOn();
                }
                else {
                    tvDevice.TurnOff();
                    deviceManager.removeDevice(tvDevice);
                }
            }
        });
    }

    private void RadiatorButtonLogic(){
        radiatorSwitchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if (radiatorSwitchButton.isChecked()) {
                    radiatorDevice = deviceFactory.getDevice("Radiator");
                    radiatorDevice.setUserId(UserId);
                    deviceManager.addDevice(radiatorDevice, radiatorSwitchButton);
                    radiatorDevice.TurnOn();
                }
                else {
                    radiatorDevice.TurnOff();
                    deviceManager.removeDevice(radiatorDevice);
                }
            }
        });
    }

    private void AcButtonLogic(){
        acSwitchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if (acSwitchButton.isChecked()) {
                    acDevice = deviceFactory.getDevice("AC");
                    acDevice.setUserId(UserId);
                    deviceManager.addDevice(acDevice, acSwitchButton);
                    acDevice.TurnOn();
                }
                else {
                    acDevice.TurnOff();
                    deviceManager.removeDevice(acDevice);
                }
            }
        });
    }

    private void rvcButtonLogic(){
        rvcSwitchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if (rvcSwitchButton.isChecked()) {
                    rvcDevice = deviceFactory.getDevice("RVC");
                    rvcDevice.setUserId(UserId);
                    deviceManager.addDevice(rvcDevice, rvcSwitchButton);
                    rvcDevice.TurnOn();
                }
                else {
                    rvcDevice.TurnOff();
                    deviceManager.removeDevice(rvcDevice);
                }
            }
        });
    }

    private void lampButtonLogic(){
        lampSwitchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if (lampSwitchButton.isChecked()) {
                    lampDevice = deviceFactory.getDevice("Lamp");
                    lampDevice.setUserId(UserId);
                    deviceManager.addDevice(lampDevice, lampSwitchButton);
                    lampDevice.TurnOn();
                }
                else {
                    lampDevice.TurnOff();
                    deviceManager.removeDevice(lampDevice);
                }
            }
        });
    }

    private void InfoButtonLogic(Map<Switch, Button> map){
        for (final Switch sw : map.keySet()) {
            map.get(sw).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (sw.isChecked()) {
                        Intent intent = new Intent(getApplicationContext(), DeviceLayout.class);
                        focusedDevice = deviceManager.getDevicebySwitch(sw);
                        startActivity(intent);
                    } else
                        Toast.makeText(getApplicationContext(), "Device " + sw.getText() + " is not turned on!", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void InitializeViews(){
        usernameHolder = findViewById(R.id.usernameHolder);
        fridgeInfoButton = findViewById(R.id.button);
        tvInfoButton = findViewById(R.id.button2);
        radiatorInfoButton = findViewById(R.id.button3);
        acInfoButton = findViewById(R.id.button4);
        rvcInfoButton = findViewById(R.id.button5);
        lampInfoButton = findViewById(R.id.button6);
        fridgeSwitchButton = findViewById(R.id.switch1);
        fridgeSwitchButton.setChecked(false);
        tvSwitchButton = findViewById(R.id.switch2);
        tvSwitchButton.setChecked(false);
        radiatorSwitchButton = findViewById(R.id.switch3);
        radiatorSwitchButton.setChecked(false);
        acSwitchButton = findViewById(R.id.switch4);
        acSwitchButton.setChecked(false);
        rvcSwitchButton = findViewById(R.id.switch5);
        rvcSwitchButton.setChecked(false);
        lampSwitchButton = findViewById(R.id.switch6);
        lampSwitchButton.setChecked(false);
    }

    private void InitializeMap(){
        viewsMap = new HashMap<>();
        viewsMap.put(fridgeSwitchButton, fridgeInfoButton);
        viewsMap.put(tvSwitchButton, tvInfoButton);
        viewsMap.put(radiatorSwitchButton, radiatorInfoButton);
        viewsMap.put(acSwitchButton, acInfoButton);
        viewsMap.put(rvcSwitchButton, rvcInfoButton);
        viewsMap.put(lampSwitchButton, lampInfoButton);
    }
}