package venckus.gytis.devicesmanagermqtt;

import android.os.StrictMode;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ServerRequests {
    String usersEndpoint = "https://overlord-iot.herokuapp.com/api/v1/users";
    String UserId;

    public ServerRequests()
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public void SendRequest_GET(String username){
        try {
            URL obj = new URL(usersEndpoint+"?username="+username);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            JSONArray myResponse = new JSONArray(response.toString());
            JSONObject jsonObject = myResponse.getJSONObject(0);
            UserId = jsonObject.getString("id");
            System.out.println("User id: " + UserId);
        } catch (Exception e){
                e.printStackTrace();
            }
    }

    public String getUserId(){
        return this.UserId;
    }
}
